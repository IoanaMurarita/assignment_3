
const FIRST_NAME = "Ioana";
const LAST_NAME = "Murarita";
const GRUPA = "1092";

/**
 * Make the implementation here
 */


class Employee {
    constructor(name, surname, salary){
        this.name=name;
        this.surname=surname;
        this.salary=salary;

    }

    getDetails(){
        
        return `${this.name} ${this.surname} ${this.salary}`;
    }


    
}

class SoftwareEngineer extends Employee {
   
    constructor(name, surname, salary, experience){
        super(name,surname, salary);
        if(experience='undefined'){
            this.experience='JUNIOR';
        }else{
            this.experience=experience;
        }
            

        
        
    }


    applyBonus(experience='JUNIOR'){
        switch(this.experience){
            case 'JUNIOR':
                return this.salary*1.1;
                break;
            case 'MIDDLE':
                return this.salary*1.15;
                break;
            case 'SENIOR':
                return this.salary*1.2;
                break;
            default:
               return this.salary*1.1;   
               break; 
        }

    }

   
}




module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}

